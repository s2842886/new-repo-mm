angular.module('starter.services', [])
/**
 * A simple example service that returns some data.
 */
.factory('fireBaseData', function($firebase) {
  var ref = new Firebase("https://brilliant-fire-2080.firebaseio.com/"),
      refStudents = new Firebase("https://brilliant-fire-2080.firebaseio.com/students")
  return {
    ref: function () {
      return ref;
    },
    refStudents: function () {
      return refStudents;
    }
  }
});