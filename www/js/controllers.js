angular.module('starter.controllers', ['ngRoute'])

.controller('StudentCtrl', function($scope, fireBaseData, $firebase, $location, $window) {
      $scope.students = $firebase(fireBaseData.refStudents()).$asArray();
        $scope.user = fireBaseData.ref().getAuth();

        $scope.studDetails = function() {
          $window.location.href = "#/menu/studentDetails";
        }

        $scope.getStudent = function() {
          return $scope.student.$id;
        }

      $scope.addStudent = function(e) {

        if($scope.studEndDate == undefined && $scope.age == "adult") {

          $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStartDate: $scope.studStartDate.toString(),
            studEndDate: $scope.studEndDate == "Not Applicable",
            studPaymentType: $scope.studPaymentType,
            studAddress: $scope.studAddress,
            Econtact: $scope.Econtact,
            Ephone: $scope.Ephone,
            age: $scope.age,
            parFirstName: $scope.parFirstName == "Not Applicable",
            parLastName: $scope.parLastName == "Not Applicable",
            parAddress: $scope.parAddress == "Not Applicable",
            parEmail: $scope.parEmail == "Not Applicable",
            parPhone: $scope.parPhone == "Not Applicable"
          })
        } 
        else if ($scope.age == "adult"){
          $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStartDate: $scope.studStartDate.toString(),
            studEndDate: $scope.studEndDate.toString(),
            studPaymentType: $scope.studPaymentType,
            studAddress: $scope.studAddress,
            Econtact: $scope.Econtact,
            Ephone: $scope.Ephone,
            age: $scope.age,
            parFirstName: $scope.parFirstName == "Not Applicable",
            parLastName: $scope.parLastName == "Not Applicable",
            parAddress: $scope.parAddress == "Not Applicable",
            parEmail: $scope.parEmail == "Not Applicable",
            parPhone: $scope.parPhone == "Not Applicable"
             }) 
        } 
        else if($scope.studEndDate == undefined && $scope.age == "underage") {
          $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStartDate: $scope.studStartDate.toString(),
            studEndDate: $scope.studEndDate == "Not Applicable",
            studPaymentType: $scope.studPaymentType,
            studAddress: $scope.studAddress,
            Econtact: $scope.Econtact,
            Ephone: $scope.Ephone,
            age: $scope.age,
            parFirstName: $scope.parFirstName,
            parLastName: $scope.parLastName,
            parAddress: $scope.parAddress,
            parEmail: $scope.parEmail,
            parPhone: $scope.parPhone
          })
        } 
        else if($scope.age == "underage") {
          $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStartDate: $scope.studStartDate.toString(),
            studEndDate: $scope.studEndDate.toString(),
            studPaymentType: $scope.studPaymentType,
            studAddress: $scope.studAddress,
            Econtact: $scope.Econtact,
            Ephone: $scope.Ephone,
            age: $scope.age,
            parFirstName: $scope.parFirstName,
            parLastName: $scope.parLastName,
            parAddress: $scope.parAddress,
            parEmail: $scope.parEmail,
            parPhone: $scope.parPhone
        })
        } 
          $scope.studFirstName = "";
          $scope.studLastName = "";
          $scope.studEmail = "";
          $scope.studPhone = "";
          $scope.studDOB = "";
          $scope.StartDate = "";
          $scope.studEndDate = "";
          $scope.studPaymentType = "";
          $scope.studAddress = "";
          $scope.Econtact = "";
          $scope.Ephone = "";
          $scope.parFirstName = "";
          $scope.parLastName = "";
          $scope.parAddress = "";
          $scope.parEmail = "";
          $scope.parPhone = ""; 
          $location.path("/menu/studentList");
      }
})

.controller('AccountCtrl', function($scope, fireBaseData, $firebase, $location, $window) {
        //$scope.showLoginForm = false;
        //Checking if user is logged in
        $scope.user = fireBaseData.ref().getAuth();
        if (!$scope.user) {
            $scope.showLoginForm = true;
        }
        //Login method
        $scope.login = function (em, pwd) {
            fireBaseData.ref().authWithPassword({
                email    : em,
                password : pwd
            }, function(error, authData) {
                if (error === null) {
                    $location.path("/menu/userAccount");
                    console.log("User ID: " + authData.uid + ", Provider: " + authData.provider);
                    console.log("Signed In");
                    $scope.user = fireBaseData.ref().getAuth();
                    $scope.$apply();
                } else {
                    alert(error);
                }
            });
        };
        //Logout method
        $scope.logout = function () {
          fireBaseData.ref().unauth();
          $location.path("/logIn");
          console.log("Successfully logged out");
          //SPITS OUT Error: permission_denied: Client doesn't have permission to access the desired data.
          //ERROR DOES NOTHING, STILL WORKS
        };
        //Register method
        $scope.register = function(em,pwd) {
            fireBaseData.ref().createUser({
                  email: em,
                  password: pwd
                  },function(error) {
                        if (error) {
                        alert(error);
                        } else {
                        alert("Registration Successful: Please Sign In");
                        }
                  });
        };
});
